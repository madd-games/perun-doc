/*
	Perun Documentation Generator (perun-doc)

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace perundoc.main;

import std.io.*;
import std.cont.*;

import perundoc.html.Document;
import perundoc.html.BlockClassList;
import perundoc.html.BlockIntro;

import perundoc.parse.PerunLexer;
import perundoc.parse.Token;
import perundoc.parse.ClassParser;

/**
 * Represents a package read from the `mbs.meta` file.
 */
public final class Package
{
	/**
	 * The class search path for this package.
	 */
	private List<Path> classPath = new LinkedList<Path>();
	
	/**
	 * Maps class names to the path to the corresponding `.per` file.
	 */
	private Map<String, Path> classCatalog = new HashMap<String, Path>();
	
	/**
	 * Name of this package.
	 */
	private String name;
	
	/**
	 * The CSS stylesheet.
	 */
	private static final String[] CSS = new String[] {
		// do a CSS reset
		"* {",
		"	margin: 0;",
		"	padding: 0;",
		"	box-sizing: border-box;",
		"}",
		
		// put a sans-serif font everywhere
		"html {",
		"	font-family: sans-serif;",
		"}",
		
		"pre {",
		"	font-size: 1.2rem;",
		"}",
		
		"pre, code {",
		"	background-color: #eee;",
		"	border-radius: 5px;",
		"	padding: 5px;",
		"}",
		
		"code {",
		"	font-size: 100%;",
		"}",
		
		"header {",
		"	display: block;",
		"	width: 100%;",
		"	background-color: #5555FF;",
		"	color: white;",
		"	padding-left: 2rem;",
		"	padding-right: 2rem;",
		"}",
		
		".header-pkg-name {",
		"	font-size: 2rem;",
		"	font-weight: bold;",
		"}",
		
		"ul, li {",
		"	box-sizing: context-box;",
		"}",
		
		"main {",
		"	padding: 1.5rem;",
		"}",
		
		"p, h1, h2, h3, h4, h5, h6 {",
		"	padding-top: 0.5rem;",
		"	padding-bottom: 0.5rem;",
		"}",
		
		"footer {",
		"	text-align: center;",
		"	font-size: 0.8rem;",
		"	color: #999999;",
		"}",
		
		// links
		"a:link, a:visited {",
		"	text-decoration: none;",
		"	color: #0000aa;",
		"}",
		
		"a:active, a:hover {",
		"	text-decoration: underline;",
		"	color: #0000aa;",
		"}",
		
		// member doc
		".member-doc {",
		"	border: 1px #99bbff solid;",
		"	border-radius: 10px;",
		"	overflow: hidden;",
		"	margin: 1rem;",
		"}",
		
		".member-doc-header {",
		"	background-color: #99bbff;",
		"	font-family: monospace;",
		"	padding: 10px;",
		"	font-size: 1.2rem;",
		"}",
		
		".member-doc-body {",
		"	padding: 15px;",
		"}",
		
		// method synopsis
		".method-defline, .method-suffix {",
		"	display: block;",
		"}",
		
		".method-arg {",
		"	display: block;",
		"	padding-left: 4em;",
		"}",
		
		".method-arg-name {",
		"	font-style: italic;",
		"}",
		
		// method data
		".method-doc-section {",
		"	font-size: 1rem;",
		"	margin-top: 1rem;",
		"}",
		
		".method-data-table {",
		"	margin-left: 2rem;",
		"}",
		
		".method-data-table td {",
		"	padding: 5px;",
		"}",
		
		".method-data-table-name {",
		"	font-weight: normal;",
		"	font-family: monospace;",
		"	text-align: left;",
		"}",
		
		".method-data-p {",
		"	margin-left: 2rem;",
		"}"
	};
	
	/**
	 * Construct the package with the specified name.
	 */
	public Package(String name)
	{
		this.name = name;
	};
	
	/**
	 * Add to the class path.
	 */
	public void addClassPath(Path path)
	{
		classPath.append(path);
	};
	
	/**
	 * Get the name of this package.
	 */
	public String getName()
	{
		return name;
	};
	
	/**
	 * Load classes into the catalog from the specified directory.
	 * 
	 * \param dir The directory from which to load classes (including its subdirectories).
	 * \param prefix The namespace prefix - "" for main dir, "namespace." for subdirs etc.
	 */
	private void doClassCatalog(Path dir, String prefix)
	{
		Iterator<Path> it;
		for (it=new FlatDirectoryIterator(dir); !it.end(); it.next())
		{
			Path path = it.get();
			String baseName = path.getBaseName();
			
			if (FileSystem.isDirectory(path))
			{
				doClassCatalog(path, prefix + baseName + ".");
			}
			else if (baseName.endsWith(".per"))
			{
				String className = prefix + baseName.substr(0, ((int)baseName.size())-4);
				classCatalog[className] = path;
			};
		};
	};
	
	/**
	 * Generate documentation for this package at the specified directory.
	 * 
	 * \param dir The directory inside of which to generate documentation.
	 */
	public void generateDocs(Path dir)
	{
		System.stderr.writeLine("Generating documentation for `" + name + "` in `" + dir + "`...");
		
		Iterator<Path> it;
		for (it=classPath.iterate(); !it.end(); it.next())
		{
			doClassCatalog(it.get(), "");
		};
		
		System.stderr.writeLine("Class catalog for `" + name + "`: ");
		
		Iterator< KeyValuePair<String, Path> > cit;
		for (cit=classCatalog.iterate(); !cit.end(); cit.next())
		{
			System.stderr.writeLine("\t" + cit.get().getKey() + " -> " + cit.get().getValue());
		};
		
		System.stderr.writeLine("Writing the stylesheet...");
		File css = File.open(dir + new Path("style.css"), FileFlags.writeable().createIfNonExistent().truncate());
		
		int i;
		for (i=0; i<CSS.length; i++)
		{
			css.writeLine(CSS[i]);
		};
		
		css.close();
		
		System.stderr.writeLine("Generating index.html...");
		
		List<String> classList = new ArrayList<String>(classCatalog.getKeySet().iterate());
		Sorter.bubbleSort(classList);
		
		Document index = new Document(name);
		index.addBlock(new BlockIntro(name, "Documentation generated by perun-doc."));
		index.addBlock(new BlockClassList(classList));
		index.generate(dir + new Path("index.html"));
		
		System.stderr.writeLine("Generating class documentation...");
		
		for (cit=classCatalog.iterate(); !cit.end(); cit.next())
		{
			System.stderr.writeLine("Generating documentation for " + cit.get().getKey() + "...");
			
			Path path = cit.get().getValue();
			File fp = File.open(path, FileFlags.readable());
			String data = "";
			String line;
			while ((line = fp.readLine()) != null)
			{
				data += line + "\n";
			};
			fp.close();
			
			PerunLexer lexer = new PerunLexer(path.toString(), data);
			Token chain = lexer.getTokenChain();
			
			Document doc = new Document(name);
			ClassParser parser = new ClassParser(chain, cit.get().getKey(), classCatalog.getKeySet());
			parser.generate(doc);
			doc.generate(dir + new Path("class-" + cit.get().getKey() + ".html"));
		};
	};
};