/*
	Perun Documentation Generator (perun-doc)

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace perundoc.parse;

import std.io.*;
import std.cont.*;

import perundoc.html.Document;
import perundoc.html.Block;
import perundoc.html.BlockTopHeading;
import perundoc.html.BlockSectionHeading;
import perundoc.html.BlockParagraph;
import perundoc.html.BlockSynopsis;
import perundoc.html.BlockMemberDoc;
import perundoc.html.ClassMember;

/**
 * Parse class files to generate documentation.
 */
public final class ClassParser
{
	/**
	 * Map shortnames to fullnames, wherever known.
	 */
	private Map<String, String> shortNameTable = new HashMap<String, String>();
	
	/**
	 * Storage class (abstract, final, static or empty string).
	 */
	private String storageClass;
	
	/**
	 * Documentation comment.
	 */
	private DocComment docComment;
	
	/**
	 * Fullname.
	 */
	private String fullname;
	
	/**
	 * List of parent classes (other than `std.rt.Object`).
	 */
	private List<TypeSpec> parents = new LinkedList<TypeSpec>();
	
	/**
	 * List of template parameters.
	 */
	private List<TemplateParameter> templParams = new LinkedList<TemplateParameter>();
	
	/**
	 * List of fields.
	 */
	private List<Field> fields = new LinkedList<Field>();
	
	/**
	 * List of methods.
	 */
	private List<Method> methods = new LinkedList<Method>();
	
	/**
	 * List of constructors.
	 */
	private List<Method> ctors = new LinkedList<Method>();
	
	/**
	 * Write an error to the console, and terminate abnormally.
	 */
	private void error(Token tok, String msg)
	{
		System.stderr.writeLine(tok.filename + ":" + tok.lineno + ": error: " + msg);
		System.exit(1);
	};
	
	/**
	 * Parse a type specification. Returns the token after.
	 */
	private Token parseTypeSpec(Token head, TypeSpec typeSpec)
	{
		typeSpec.shortname = head.value;
		Token tok = head.next;
		
		if (tok.value == "<")
		{
			do
			{
				tok = tok.next;
				
				TypeSpec sub = new TypeSpec();
				tok = parseTypeSpec(tok, sub);
				typeSpec.templArgs.append(sub);
			} while (tok.value == ",");
			
			if (tok.value != ">")
			{
				error(tok, "expected `>' or `,' not `" + tok.value + "'");
			};
			
			tok = tok.next;
		};
		
		while (tok.value == "[]")
		{
			typeSpec.arrayDepth++;
			tok = tok.next;
		};
		
		return tok;
	};
	
	/**
	 * Skip over braces.
	 */
	private Token skipOverBraces(Token chain)
	{
		int level = 1;
		
		Token tok;
		for (tok=chain.next; tok.type!=Token.END; tok=tok.next)
		{
			if (tok.value == "{")
			{
				level++;
			}
			else if (tok.value == "}")
			{
				level--;
				if (level == 0) return tok.next;
			};
		};
		
		return tok;
	};
	
	/**
	 * Import an entire namespace into the short name table.
	 */
	private void importWildcard(Container<String> classCatalog, String namespaceName)
	{
		String prefix = namespaceName + ".";
		
		Iterator<String> it;
		for (it=classCatalog.iterate(); !it.end(); it.next())
		{
			String fullname = it.get();
			if (fullname.startsWith(prefix))
			{
				String shortName = fullname.substr(prefix.size());
				if (shortName.find(".") == -1)
				{
					shortNameTable[shortName] = fullname;
				};
			};
		};
	};
	
	/**
	 * Constructor.
	 */
	public ClassParser(Token chain, String expectedFullname, Container<String> classCatalog)
	{
		Token tok = chain;
		this.fullname = expectedFullname;
		
		if (tok.value != "namespace")
		{
			error(tok, "expected `namespace' not `" + tok.value + "'");
		};
		
		tok = tok.next;
		
		if (tok.type != Token.ID)
		{
			error(tok, "expected an identifier after `namespace'");
		};
		
		String namespaceName = tok.value;
		tok = tok.next;
		
		while (tok.value != ";")
		{
			if (tok.value != ".")
			{
				error(tok, "expected `.' or `;' not `" + tok.value + "'");
			};
			
			tok = tok.next;
			if (tok.type != Token.ID)
			{
				error(tok, "expected an identifier not `" + tok.value + "'");
			};
			
			namespaceName += "." + tok.value;
			tok = tok.next;
		};
		tok = tok.next;		// skip over ';'
		
		// follow all rules, including trying to import std.rt.*; this is in case we are
		// documenting libperun itself!
		importWildcard(classCatalog, "std.rt");
		importWildcard(classCatalog, namespaceName);
		
		while (tok.value == "import")
		{
			tok = tok.next;
			if (tok.type != Token.ID)
			{
				error(tok, "expected an identifier after `import' not `" + tok.value + "'");
			};
			
			String importFullname = tok.value;
			bool wildcard = false;
			tok = tok.next;
			
			while (tok.value == ".")
			{
				tok = tok.next;
				
				if (tok.value == "*")
				{
					wildcard = true;
					tok = tok.next;
					break;
				}
				else if (tok.type != Token.ID)
				{
					error(tok, "expected an identifier or `*', not `" + tok.value + "'");
				};
				
				importFullname += "." + tok.value;
				tok = tok.next;
			};
			
			if (tok.value != ";")
			{
				error(tok, "expected `;' not `" + tok.value + "'");
			};
			tok = tok.next;		// skip over ';'
			
			if (wildcard)
			{
				importWildcard(classCatalog, importFullname);
			}
			else
			{
				String[] tokens = importFullname.split(".");
				String lastToken = tokens[tokens.length-1];
				shortNameTable[lastToken] = importFullname;
			};
		};
		
		while (tok.type != Token.END)
		{
			if (tok.value == ";")
			{
				tok = tok.next;
				continue;
			};
			
			DocComment docComment = null;
			if (tok.type == Token.DOC)
			{
				docComment = new DocComment(tok.value);
				tok = tok.next;
			};
			
			if (tok.value != "public")
			{
				// something other than a public class, ignore it
				while (tok.value != "{" && tok.type != Token.END) tok = tok.next;
				tok = skipOverBraces(tok);
			}
			else
			{
				tok = tok.next;
				
				storageClass = "";
				if (tok.value == "abstract" || tok.value == "static" || tok.value == "final")
				{
					storageClass = tok.value;
					tok = tok.next;
				};
				
				if (tok.value != "class")
				{
					error(tok, "expected `class' not `" + tok.value + "'");
				};
				tok = tok.next;
				
				String fullname = namespaceName + "." + tok.value;
				String classShortName = tok.value;
				if (fullname != expectedFullname)
				{
					error(tok, "expected fullname `" + expectedFullname + "' but got `" + fullname + "'");
				};
				
				tok = tok.next;
				
				this.docComment = docComment;
				
				if (tok.value == "<")
				{
					do
					{
						tok = tok.next;
						
						if (tok.type != Token.ID)
						{
							error(tok, "expected an identifier, not `" + tok.value + "'");
						};
						
						String paramName = tok.value;
						tok = tok.next;
						
						TypeSpec minBase = null;
						if (tok.value == "extends")
						{
							tok = tok.next;
							
							minBase = new TypeSpec();
							tok = parseTypeSpec(tok, minBase);
						};
						
						templParams.append(new TemplateParameter(paramName, minBase));
					} while (tok.value == ",");
					
					if (tok.value != ">")
					{
						error(tok, "expected `<' or `,' not `" + tok.value + "'");
					};
					
					tok = tok.next;
				};
				
				if (tok.value == "extends")
				{
					do
					{
						tok = tok.next;

						TypeSpec parent = new TypeSpec();
						tok = parseTypeSpec(tok, parent);
						parents.append(parent);
					} while (tok.value == ",");
				};
				
				if (tok.value != "{")
				{
					error(tok, "expected `{' not `" + tok.value + "'");
				};
				tok = tok.next;
				
				while (tok.value != "}")
				{
					if (tok.value == ";")
					{
						tok = tok.next;
						continue;
					};
					
					if (tok.value == "destructor")
					{
						tok = tok.next;
						tok = skipOverBraces(tok);
						continue;
					};
					
					bool internal = false;
					DocComment memberDoc = null;
					if (tok.type == Token.DOC)
					{
						memberDoc = new DocComment(tok.value);
						tok = tok.next;
					};
					
					String accessQualif;
					if (tok.value != "private" && tok.value != "protected" && tok.value != "public")
					{
						error(tok, "expected an access qualifier, not `" + tok.value + "'");
					};
					accessQualif = tok.value;
					tok = tok.next;
					
					if (accessQualif == "private")
					{
						internal = true;
					};
					
					String defLine = accessQualif;
					while (tok.value == "static" || tok.value == "final" || tok.value == "abstract" || tok.value == "override")
					{
						if (tok.value != "override") defLine += " " + tok.value;
						tok = tok.next;
					};
					
					bool isCtor;
					if (tok.value == classShortName && tok.next.value == "(")
					{
						defLine += " <strong>" + classShortName + "</strong>";
						tok = tok.next;
						isCtor = true;
					}
					else
					{
						isCtor = false;
						
						TypeSpec type = new TypeSpec();
						tok = parseTypeSpec(tok, type);
						
						defLine += " " + type.format(shortNameTable);
						
						if (tok.type != Token.ID)
						{
							error(tok, "expected an identifier, not `" + tok.value + "'");
						};
						
						if (tok.value.startsWith("_"))
						{
							internal = true;
						};

						defLine += " " + tok.value;
						tok = tok.next;
					};
					
					if (tok.value == "=")
					{
						while (tok.value != ";" && tok.type != Token.END) tok = tok.next;
					};
					
					if (tok.value == ";")
					{
						Field field = new Field(memberDoc, defLine);
						field.internal = internal;
						fields.append(field);
					}
					else if (tok.value == "(")
					{
						Method method = new Method(memberDoc, defLine);
						method.internal = internal;
						
						if (isCtor) ctors.append(method);
						else methods.append(method);
						
						if (tok.next.value == ")")
						{
							tok = tok.next.next;
						}
						else
						{
							do
							{
								tok = tok.next;
								
								TypeSpec type = new TypeSpec();
								tok = parseTypeSpec(tok, type);
								
								if (tok.type != Token.ID)
								{
									error(tok, "expected an identifier, not `" + tok.value + "'");
								};
								
								String name = tok.value;
								tok = tok.next;
								
								String line = type.format(shortNameTable) + " <span class=\"method-arg-name\">"
									+ name + "</span>";
								if (tok.value == ",") line += ",";
								
								method.addArg(line);
							} while (tok.value == ",");
							
							if (tok.value != ")")
							{
								error(tok, "expected `)' or `,' not `" + tok.value + "'");
							};
							
							tok = tok.next;
						};
						
						if (tok.value == ":")
						{
							while (tok.value != "{" && tok.type != Token.END) tok = tok.next;
						};
						
						if (tok.value == "extern")
						{
							tok = tok.next;
							
							if (tok.type != Token.ID)
							{
								error(tok, "expected an identifier, not `" + tok.value + "'");
							};
							
							tok = tok.next;
						};
						
						if (tok.value == "{")
						{
							tok = skipOverBraces(tok);
						};
					}
					else
					{
						error(tok, "expected `;' or `(' not `" + tok.value + "'");
					};
				};
				
				return;
			};
		};
	};
	
	/**
	 * Generate a section documenting a set of members.
	 */
	public void outputMemberDocs(Document doc, String title, Container<ClassMember> members)
	{
		doc.addBlock(new BlockSectionHeading(title));
		
		Iterator<ClassMember> it;
		for (it=members.iterate(); !it.end(); it.next())
		{
			if (!it.get().internal) doc.addBlock(new BlockMemberDoc(it.get()));
		};
	};
	
	/**
	 * Generate documentation.
	 */
	public void generate(Document doc)
	{
		String[] tokens = fullname.split(".");
		String shortName = tokens[tokens.length-1];
		
		doc.addBlock(new BlockTopHeading("Class " + fullname));
		doc.addBlock(new BlockSynopsis(storageClass, shortName, templParams, parents, shortNameTable));
		
		if (docComment == null)
		{
			doc.addBlock(new BlockParagraph("No description given"));
		}
		else
		{
			Iterator<String> it;
			for (it=docComment.iterateParagraphs(); !it.end(); it.next())
			{
				doc.addBlock(new BlockParagraph(it.get()));
			};
		};
		
		outputMemberDocs(doc, "Fields", fields);
		outputMemberDocs(doc, "Constructors", ctors);
		outputMemberDocs(doc, "Methods", methods);
	};
};