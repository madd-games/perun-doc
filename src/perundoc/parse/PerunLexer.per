/*
	Perun Documentation Generator (perun-doc)

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace perundoc.parse;

import std.re.*;

/**
 * Lexer for Perun code.
 */
public final class PerunLexer extends Lexer
{
	private Object ttKeyword;
	private Object ttType;
	private Object ttConst;
	private Object ttOp;
	private Object ttID;
	private Object ttDoc;
	
	private String filename;
	private Token tokFirst;
	private Token tokLast;
	
	/**
	 * List of all keywords.
	 */
	private static final String[] KEYWORDS = new String[] {
		"namespace",
		"import",
		"private",
		"protected",
		"public",
		"static",
		"abstract",
		"final",
		"override",
		"class",
		"extends",
		"true",
		"false",
		"null",
		"is",
		"new",
		"this",
		"if",
		"else",
		"while",
		"for",
		"break",
		"continue",
		"return",
		"try",
		"catch",
		"throw",
		"do",
		"destructor",
		"extern",
		"const",
		"export",
		"switch",
		"case",
		"default",
		"foreach",
		"enum",
		"struct",
		"auto"
	};
	
	/**
	 * List of all primitive types.
	 */
	private static final String[] TYPES = new String[] {
		"void",
		"bool",
		"float",
		"double",
		"byte_t",
		"sbyte_t",
		"word_t",
		"sword_t",
		"dword_t",
		"sdword_t",
		"qword_t",
		"sqword_t",
		"int",
		"long",
		"ptr_t",
		"sptr_t"
	};
	
	/**
	 * List of all operators.
	 */
	private static final String[] OPS = new String[] {
		"\\<\\<\\=",
		"\\>\\>\\=",
		"\\<\\<",
		"\\>\\>",
		"\\[\\]",
		"\\+\\=",
		"\\&\\=",
		"\\/\\=",
		"\\%\\=",
		"\\*\\=",
		"\\|\\=",
		"\\-\\=",
		"\\^\\=",
		"\\|\\|",
		"\\&\\&",
		"\\=\\=",
		"\\!\\=",
		"\\<\\=",
		"\\>\\=",
		"\\+\\+",
		"\\-\\-",
		"\\.",
		"\\,",
		"\\;",
		"\\:",
		"\\<",
		"\\>",
		"\\{",
		"\\}",
		"\\*",
		"\\/",
		"\\%",
		"\\(",
		"\\)",
		"\\=",
		"\\?",
		"\\|",
		"\\&",
		"\\^",
		"\\+",
		"\\-",
		"\\~",
		"\\!",
		"\\[",
		"\\]",
		"\\$"
	};
	
	/**
	 * Constructor. Takes the name of a file to parse, and the contents.
	 * 
	 * \param filename The file name (to be used in error messages).
	 * \param data The file contents.
	 */
	public PerunLexer(String filename, String data)
	{
		this.filename = filename;
		
		// normal tokens
		ttKeyword = addTokenType("(" + "|".join(KEYWORDS) + ")");
		ttType = addTokenType("(" + "|".join(TYPES) + ")");
		ttConst = addTokenType("((0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)|(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)[uU]|(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)[lL]|(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)[uU][lL]|[0-9]*\\.[0-9]+([eE][-+][0-9]+)?|[0-9]*\\.[0-9]+([eE][-+][0-9]+)?[Ff]|\\\"(\\\\.|.)*?\\\")");
		ttOp = addTokenType("(" + "|".join(OPS) + ")");
		ttID = addTokenType("[_a-zA-Z][_0-9a-zA-Z]*");
		ttDoc = addTokenType("\\/\\*\\*.*?(\\*\\/)");
		
		// whitespace tokens
		addWhitespaceTokenType("$+");
		addWhitespaceTokenType("\\/\\*.*?(\\*\\/)");
		addWhitespaceTokenType("\\/\\/.*?\n");
		
		feed(data);
		
		Token end = new Token();
		end.filename = filename;
		end.lineno = data.count("\n");
		end.value = "<EOF>";
		end.next = end;
		
		if (tokFirst == null)
		{
			tokFirst = tokLast = end;
		}
		else
		{
			tokLast.next = end;
		};
	};
	
	protected override void onToken(Object type, int lineno, Match match)
	{
		Token tok = new Token();
		tok.value = match.getValue();
		tok.lineno = lineno;
		tok.filename = filename;
		
		if (type == ttKeyword)
		{
			tok.type = Token.KEYWORD;
		}
		else if (type == ttType)
		{
			tok.type = Token.TYPE;
		}
		else if (type == ttConst)
		{
			tok.type = Token.CONST;
		}
		else if (type == ttOp)
		{
			tok.type = Token.OP;
		}
		else if (type == ttID)
		{
			tok.type = Token.ID;
		}
		else if (type == ttDoc)
		{
			tok.type = Token.DOC;
		}
		else
		{
			System.assert(false);
		};
		
		if (tokFirst == null)
		{
			tokFirst = tokLast = tok;
		}
		else
		{
			tokLast.next = tok;
			tokLast = tok;
		};
	};
	
	/**
	 * Return the chain of tokens.
	 */
	public Token getTokenChain()
	{
		return tokFirst;
	};
};